# ploy_puppet

This is a ploy plugin for configuring jailhosts and jails with Puppet.

## Why Puppet?

Puppet is a very mature CM system, and has a swath of modules many of which
support FreeBSD. While Puppet is generally targeted to running continuously, it
can easily be used for the kind of one-shot configurations we want in Ploy.

Similarly to Ansible, our work-directory serves as a configuration base, and as
drop-off for modules, as Hiera store, etc…

## How?

In order to keep our Cloud Provider costs low, and mimic the patterns laid forth
by Ploy Ansible, we will run puppet server-less.

The remote puppet installation is used to gather facts (analogous to Ansible
Inventory), while the local (or where ever our provisioning host is) puppet
installation is used to compile the catalog.
This mode of operations means that we won't need any data (and hence, no
secrets) on the Jailhost or the Jails, and also no puppet modules.

## How??

In a first step, we gather facts (the Inventory).

```
for i in instances:
  ssh i facter -y > var/i.yaml
```

Aside from the facts, the catalog will also be influenced by our manifest,
and Hiera configuration.

The default manifest is simply:

```puppet
include hiera(roles, [])
```

Which, again, mimics the default behaviour of Ploy Ansible, but is also very
close to the Roles & Profiles pattern most Puppet users will be intimately
familiar with.

If we stick with this default manifest, our Hiera config will look very similar
to the Ansible Host Vars:

```yaml
# configure web server
---
roles:
 - apache

apache::listen: 8080
apache::vhosts:
  example.com:
    documentroot: /usr/local/var/www
```

With everything in place, we can now compile a catalog, and send it off to the
instance:

```
puppet master --compile --manifest manifests/site.pp --vardir var <instance> | \
  ssh <instance> puppet apply --catalog
```
